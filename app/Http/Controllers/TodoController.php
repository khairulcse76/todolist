<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{

    public function index()
    {
        $todos=Todo::all();
        return view('todo.home', compact('todos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('todo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $todo=new Todo();
        $this->validate($request,[
           'title'=>'required|unique:todos',
           'body'=>'required',
        ]);
        $todo->title=$request->title;
        $todo->body=$request->body;
        if ($todo->save()){
            session()->flash('massage', 'Data inserted successful');
        }
        return redirect('/todo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $items=Todo::find($id);
        return view('todo.show', compact('items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $items=Todo::find($id);
        return view('todo.edit', compact('items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $todo=Todo::find($id);
        $this->validate($request,[
            'title'=>'required',
            'body'=>'required',
        ]);
        $todo->title=$request->title;
        $todo->body=$request->body;
        if ($todo->save()){
            session()->flash('massage', 'Update successful');
        }
        return redirect('/todo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items=Todo::find($id);
        if ($items->delete()){
            session()->flash('massage', 'Delete successful');
        }
        return redirect('/todo');
    }
}
