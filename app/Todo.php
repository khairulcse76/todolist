<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    public function getBodyattribute($value){

        return ucfirst($value);
    }

    public function setBodyattribute($value){
        return $this->attributes['body']=ucfirst($value);
    }
}

