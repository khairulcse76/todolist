<div class="alert-danger">
    @if(count($errors)>0)
        @foreach($errors->all() as $error)
            <span class="alert-danger text-center">{{ $error }}</span><hr/>
        @endforeach

    @endif </div>