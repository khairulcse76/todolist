
@if(session()->has('massage'))
    <div class="alert alert-success alert-dismissible">
        <a href="/todo" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <center>{{ session()->get('massage') }}</center>
    </div>
@endif
