@extends('layouts.app')
@section('body')
    <br>
    <a href="/todo" class="btn btn-info">Back</a>
    <div class="col-lg-4 col-lg-offset-4">
        <h2>{{ ucfirst(substr(Route::currentRouteName(),5)) }} list</h2>
        <form class="form-horizontal" action="/todo/@yield('editId')" method="post">
            {{ csrf_field() }}
            @section('editMethod')
                @show
                <div class="form-group">
                    <div class="col-lg-10">
                        <input class="form-control" name="title" value="@yield('editTitle')" id="title" placeholder="Please Enter title hare" type="text"/>
                        <textarea class="form-control" name="body" rows="5" id="textArea" placeholder="Please Enter somthing">@yield('editBody')</textarea>
                        <br>
                        <button type="reset" class="btn btn-default">Reset</button>
                        <button type="submit" class="btn btn-success">Submit @yield('editSubmit')</button>
                    </div>
                </div>

            </fieldset>
        </form>
        @include('includes.errors');
    </div>
@endsection
