@extends('todo.create')

@section('editId', $items->id)
@section('editTitle', $items->title)
@section('editBody', $items->body)
@section('editMethod')
    {{ method_field('put') }}
@endsection
@section('editSubmit', '/ Update')
