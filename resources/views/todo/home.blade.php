@extends('layouts.app')
@section('body')
    <br>
    @include('includes.success')
    <a href="todo/create" class="btn btn-info">Add New</a>
    <div class="col-lg-8 col-lg-offset-3">
        <center><h2>Todo List</h2></center>
        <ul class="list-group col-lg-8">
            @foreach($todos as $todo)
                <li class="list-group-item">
                    <a href="{{'/todo/'.$todo->id }}" >{{ $todo->title }}</a>
                        {{--{{ ucfirst($todo->body) }}--}}
                        <span class="pull-right">{{ $todo->created_at->diffForHumans() }}</span>
                </li>
            @endforeach
        </ul>
        <ul class="list-group col-lg-4">
            @foreach($todos as $todo)
                <li class="list-group-item">
                    <a href="{{'/todo/'.$todo->id }}" ><span class="glyphicon glyphicon-eye-open"></span></a>--||--
                    <a href="{{'/todo/'.$todo->id.'/edit' }}" ><span class="glyphicon glyphicon-edit"></span></a>--||--
                    <form action="{{'/todo/'.$todo->id }}"  class="form-group pull-right" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button style="border:none;"><span class="glyphicon glyphicon-trash"></span></button>
                    </form>
                </li>
            @endforeach
        </ul>
    </div>
@endsection













