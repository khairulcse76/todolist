@extends('layouts.app')
@section('body')
    <br>
    <a href="/todo" class="btn btn-info">Back</a>
    <div class="col-lg-6 col-lg-offset-4">
        <center><h2>Todo List Details</h2></center>

        <ul class="list-group">
            <li class="list-group-item">
                {{ $items->body }}
                {{--{{ ucfirst($todo->body) }}--}}
                <span class="pull-right">{{ $items->created_at->diffForHumans() }}</span>
            </li>
        </ul>
    </div>
@endsection
